#
# Cookbook Name:: ychefdk
# Recipe:: default
#
# Copyright (c) 2016 Tsang Yong, All Rights Reserved.

directory "/packages"

remote_file "/packages/chefdk.deb" do
  source node["chefdk"]["package_url"]
  owner "root"
  group "root"
  mode "0644"
  action :create
end

dpkg_package "chefdk" do
  source "/packages/chefdk.deb"
  action :install
end
