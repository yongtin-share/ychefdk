module YChefDKCookbook
  module Resource
    class YChefDKKnifeConfigureResource < Chef::Resource::LWRPBase
      self.resource_name = :knife_configure
      provides(:knife_configure)
      default_action(:create)
      actions(:create)

      attribute(:user, kind_of: String, :required => true)
      attribute(:home_dir, kind_of: String, :required => true)
      attribute(:private_key, kind_of: String, :required => true)
      attribute(:validator_key, kind_of: String, :required => true)
      attribute(:validator_name, kind_of: String, :required => true)
      attribute(:chef_server_url, kind_of: String, :required => true)
      attribute(:chef_name, kind_of: String, :required => true)
      attribute(:trusted_certs, kind_of: String, :default => "")
    end
  end

  module Provider
    class YChefDKKnifeConfigureProvider < Chef::Provider::LWRPBase
      use_inline_resources
      provides(:knife_configure)

      action :create do
        directory "#{new_resource.home_dir}/.chef" do
          owner new_resource.user
          mode 0755
        end
        directory "#{new_resource.home_dir}/.chef/trusted_certs" do
          owner new_resource.user
          mode 0755
        end if new_resource.trusted_certs != ""
        file "#{new_resource.home_dir}/.chef/trusted_certs/#{new_resource.chef_name}.crt" do
          mode 0644
          owner new_resource.user
          content new_resource.trusted_certs
          sensitive true
        end
        file "#{new_resource.home_dir}/.chef/#{new_resource.name}-#{new_resource.chef_name}.pem" do
          mode 0600
          owner new_resource.user
          content new_resource.private_key
          sensitive true
        end
        file "#{new_resource.home_dir}/.chef/#{new_resource.validator_name}.pem" do
          mode 0600
          owner new_resource.user
          content new_resource.validator_key
          sensitive true
        end
        template "#{new_resource.home_dir}/.chef/knife.rb" do
          mode 0644
          owner new_resource.user
          source "knife-configure.rb.erb"
          cookbook "ychefdk"
          variables(
            :node_name => new_resource.name,
            :client_key => "#{new_resource.home_dir}/.chef/#{new_resource.name}-#{new_resource.chef_name}.pem",
            :validation_client_name => new_resource.validator_name,
            :validation_key => "#{new_resource.home_dir}/.chef/#{new_resource.validator_name}.pem",
            :chef_server_url => new_resource.chef_server_url,
            :syntax_check_cache_path => "#{new_resource.home_dir}/.chef/syntax_check_cache"
          )
        end

      end
    end
  end
end
